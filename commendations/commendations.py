from redbot.core import Config, checks, commands
from redbot.core.commands import Cog
from redbot.core.utils import menus
import asyncio
import discord
from discord import Embed
import hashlib
import os
import re

class SameUserError(Exception):
    pass

class Commendations(Cog):
    """Commendations

    Allows users to commend eachother, and keeps a running tally of each user's commendations
    """

    def __init__(self, bot):
        self.bot = bot
        self.c_commendations = {}
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "commendations": {}
        })
        asyncio.ensure_future(self.load_commendations())

    async def load_commendations(self):
        self.c_commendations = await self.config.get_raw("commendations")

    async def save_commendations(self):
        await self.config.set_raw("commendations",value=self.c_commendations)

    async def store(self, ctx, commendation):
        user_id = str(commendation['user.id'])
        guild_id = str(ctx.guild.id)
        if guild_id not in self.c_commendations:
            self.c_commendations[guild_id] = {}
        server_commendations = self.c_commendations[guild_id]
        if user_id not in server_commendations:
            server_commendations[user_id] = []
        user_commendations = server_commendations[user_id]
        user_commendations.append(commendation)
        server_commendations[user_id] = user_commendations
        self.c_commendations[guild_id] = server_commendations
        await self.save_commendations()

    def commendation(self, author, user, text):
        if author.id == user.id:
            raise SameUserError()
        comm = {}
        comm['user'] = user.name
        comm['user.id'] = user.id
        comm['text'] = text
        comm['author.id'] = author.id
        comm['author'] = author.name
        return comm

    @commands.command(name="commend")
    @commands.guild_only()
    async def commend(self, context, user: discord.Member, *, text = ""):
        """
        Adds a commendation to the given member, with the provided text as the reason behind it
        """
        author = context.message.author
        try:
            commendation = self.commendation(author, user, text)
            await self.store(context, commendation)
            await context.send("Commended {}.".format(user.name))
        except SameUserError:
            await context.send("You can't give yourself a commendation you nitwit")

    @commands.group(name = "commendations")
    async def commendations(self, context):
        if context.invoked_subcommand is None:
            await context.send_help()
            return

    def commendations_for_user(self, user):
        # server_id = str(server.id)
        # if server_id not in self.c_commendations:
        #     return []
        #     for
        user_comms = []
        for server_id in self.c_commendations:
            server_comms = self.c_commendations[server_id]
            user_id = str(user.id)
            if user_id in server_comms:
                user_comms.extend(server_comms[user_id])
        return user_comms

    @commendations.command(name = "list")
    async def list(self, context, user: discord.Member):
        """
        Provides the number of commendations the given user has received
        """
        user_comms = self.commendations_for_user(user)
        if not user_comms:
            await context.send("No commendations found for {}".format(user.name))
        await context.send("{} has {} commendations".format(user.name, len(user_comms)))

    @commendations.command(name = "detail")
    async def detail(self, context):
        """Sends you a DM with your commendations listed out"""
        user_comms = self.commendations_for_user(context.message.author)
        if not user_comms:
            await context.message.author.send("Could not find any commendations for you :(")
            return
        it = iter(user_comms)
        embeds = []
        limit = 10
        count = 0
        while True:
            embed = Embed(title="Commendations for {}".format(context.author.name), type="rich")
            added = 0
            try:
                for _ in range(limit):
                    commendation = next(it)
                    count += 1
                    added += 1
                    embed.add_field(name = str(count) + ": " + commendation['text'], value = commendation['author'])
                embeds.append(embed)
            except StopIteration:
                if added > 0:
                    embeds.append(embed)
                break

        await menus.menu(context, embeds, menus.DEFAULT_CONTROLS)

    @commendations.command(name = "leaderboard")
    @commands.guild_only()
    async def leaderboard(self, context):
        """
        Returns a leaderboard of the top 10 commendees on your server.
        """
        server_id = str(context.guild.id)
        if server_id not in self.c_commendations:
            await context.send("No commendations on this server yet")
            return
        commended_users = self.topCommendees(self.c_commendations[server_id], 10)
        leaders=[]
        rank=1
        for user_id, count in commended_users:
            try:
                user = await context.guild.fetch_member(user_id)
                leaders.append("#{} {}: {}".format(rank, user.name, count))
                rank += 1
            except Exception as e:
                print("Commendations - Could not find user with id [" + user_id + "]. Skipping. ")
                print(e)
        message = """
        Top 10 Commendees
        ```{}```
        """.format("\n".join(leaders))
        await context.send(message)

    def topCommendees(self, commendation_dict, amount):
        dic_to_list = lambda dic: [(k, len(v)) for (k, v) in dic.items()]
        commendation_counts = dic_to_list(commendation_dict)
        commendation_counts = sorted(commendation_counts, key=lambda x: x[1], reverse=True)
        return commendation_counts[:amount+1]

def setup(bot):
    bot.add_cog(Commendations(bot))
