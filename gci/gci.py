import asyncio
import os
import discord
import math
import time
import sys
import hashlib
from redbot.core import Config, checks, commands
from redbot.core.commands import Cog


GCI_SERVERS = ["GAW", "PGAW", "TNN", "MTM"]


class GCI(Cog):
    """
    Tracks active GCIs on hoggit
    """

    def __init__(self, bot):
        self.bot = bot
        self.stats = {}
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "details": {
                "active_gcis": [],
                "active_role_id": -1,
                "stats": {}
            }
        })
        self.active_role = None
        self.killSwitch = False
        self.active_time = 60 * 30 #30 minutes.
        self.warn_time = 60 * 25#25 minutes
        self.final_warning = 60 * 28
        self.reminded = []
        self.final_reminded = []
        self.guild_id = "269535523922509826" #hoggit
        asyncio.ensure_future(self.load_config())
        asyncio.ensure_future(self.update_roles())
        asyncio.ensure_future(self.start_monitor())

    async def load_config(self):
        self.active_gcis = await self.config.get_raw("details", "active_gcis")
        self.stats = await self.config.get_raw("details", "stats")
        self.active_role_id = await self.config.get_raw("details", "active_role_id")


    def __unload(self):
        log("Setting killswitch to True!")
        self.killSwitch = True


    async def get_member_by_user_id(self, userId):
        guild = await self.bot.fetch_guild(self.guild_id)
        if not guild:
            log("Tried to find hoggit server but couldn't")
            return None
        try:
            member = await guild.fetch_member(userId)
        except discord.errors.NotFound:
            member = None
        if not member:
            log("Tried to find user on hoggit but couldn't: %s" % userId)
            return None
        return member

    async def update_roles(self):
        await self.bot.wait_until_ready()
        if self.active_role_id != -1:
            guild = await self.bot.fetch_guild(self.guild_id)
            roles = await guild.fetch_roles()
            role = next(r for r in roles if r.id == self.active_role_id)
            if role:
                log("Active role was set.")
                self.active_role = role
            else:
                log("Active role wasn't valid. Not setting.")
                self.active_role = None
        else:
            log("Active role not in data. don't load it")
            self.active_role = None

        if self.active_role is not None:
            return

    async def start_monitor(self):
        await self.bot.wait_until_ready()
        if self.killSwitch:
            log("Killswitch hit. Not re-polling")
            return
        try:
            for gci in self.active_gcis:
                user = await self.get_member_by_user_id(gci['user'])
                if not user:
                    log("Couldn't find user with id " + str(gci['user']) + ". Skipping reminder.")
                    self.active_gcis[:] = [_gci for _gci in self.active_gcis if _gci['user'] != gci['user']]
                    continue
                if gci['refresh_time'] + self.active_time < time.time():
                    await user.send("30 minute duration achieved. Signing off.")
                    await self.midnight(user)
                elif gci['refresh_time'] + self.warn_time < time.time():
                    if gci['user'] not in self.reminded:
                        await user.send("You have been active as GCI for 25 minutes, in 5 minutes you will be automatically signed-off. To continue for another 30 minutes, use !gci refresh")
                        self.reminded.append(gci['user'])
                elif gci['refresh_time'] + self.final_warning < time.time():
                    if gci['user'] not in self.final_reminded:
                        await user.send("You have been active as GCI for 28 minutes, in 2 minutes you will be automatically signed-off. To continue for another 30 minutes, use !gci refresh")
                        self.final_reminded.append(gci['user'])
        except Exception as e:
            log("Unexpected error with the gci monitor: " + str(e))
        finally:
            await asyncio.sleep(60)
            asyncio.ensure_future(self.start_monitor())


    async def clear_active_role(self,user):
        if self.active_role:
            log("Active role is available. Unset {} role on {}".format(user.name, self.active_role))
            await user.remove_roles(self.active_role)
        else:
            log("Active role is not set. Skipping")

    async def add_active_role(self,user):
        if self.active_role:
            log("Active role is available. Setting {} role on {}".format(user.name, self.active_role))
            await user.add_roles(self.active_role)
        else:
            log("Active role is not set. Skipping")

    def remove_reminded_status(self, user):
        self.reminded[:] = [user_id for user_id in self.reminded if user_id != user.id]
        self.final_reminded[:] = [user_id for user_id in self.reminded if user_id != user.id]

    async def save_data(self):
        await self.config.set_raw("details","active_gcis",value=self.active_gcis)
        await self.config.set_raw("details","stats",value=self.stats)
        await self.config.set_raw("details","active_role_id",value=self.active_role_id)

    async def midnight(self, user):
        await self.clear_active_role(user)
        self.remove_reminded_status(user)
        self.active_gcis[:] = [gci for gci in self.active_gcis if gci['user'] != user.id]
        await self.save_data()

    async def sunrise(self, user, server, remarks):
        log("Adding {} as GCI".format(user.name))
        gci = {}
        gci['user'] = user.id
        gci['start_time'] = time.time()
        gci['refresh_time'] = time.time()
        gci['server'] = server
        gci['remarks'] = remarks
        self.active_gcis.append(gci)
        await self.save_data()
        await self.add_active_role(user)
        log("Added Active Role to {}".format(user.name))


    def duration(self, user):
        """Returns the duration in minutes that the user was GCI for"""
        if user['start_time']:
            now = time.time()
            duration = now - user['start_time']
            return duration
        log("Couldn't determine duration of provided user's GCI service. Returning None.")
        return None

    async def save_user_duration(self, user_id, duration):
        old_duration = self.stats.get(user_id, 0)
        self.stats[user_id] = int(old_duration + duration)
        log("updating user time from %s to %s" % (old_duration, old_duration + duration))
        await self.save_data()

    @commands.group(name="gci", no_pm=True, invoke_without_command=True, aliases=["GCI"])
    async def _gci(self, context):
        """List active GCIs"""
        if len(self.active_gcis) == 0:
            response = "No human GCIs currently online **on any server**."
        else:
            response = "Current GCIs online:\n"
            for gci in self.active_gcis:
                user = await self.get_member_by_user_id(gci['user'])
                if not user:
                    log("Couldn't find user with id " + str(gci['user']) + ". Skipping in list.")
                    continue
                response += "{} ({}) - {}\n".format(
                                user.name,
                                gci['server'],
                                gci['remarks'] if gci['remarks'] else "No remarks"
                                )
        # response += "\nBot GCI - See instructions at <https://gitlab.com/overlord-bot/srs-bot/-/wikis/Interaction>"
        # response += "\nOverlordBot (%s) - Overlord on 136.000 AM & 40.000 FM" % ", ".join(GCI_SERVERS)
        await context.send(response)

    @_gci.command(name="active_role")
    @checks.mod_or_permissions(manage_guild=True)
    async def _active_role(self, context, role: discord.Role):
        """Defines the role that active GCIs get granted. Staff Only"""
        self.active_role_id = role.id
        await self.save_data()
        await self.update_roles()
        await context.send("Set Active GCI role to: {}".format(role.name))

    @_gci.command(name="refresh", aliases=["REFRESH"])
    async def _refresh(self, context):
        """Refreshes your timer back to 30 minutes"""
        author = context.message.author
        for gci in self.active_gcis:
            if gci['user'] == author.id:
                self.remove_reminded_status(author)
                gci['refresh_time'] = time.time()
                await author.send("Refreshed your GCI timer for another 30 minutes")
                await self.save_data()
                return
        await author.send("Doesn't look like you were signed up as GCI yet. Use !gci sunrise <server>")


    @_gci.command(name="midnight", no_pm=True, aliases=["MIDNIGHT", "sunset", "SUNSET"])
    async def _midnight(self, context):
        """Removes you from the list of active GCIs."""
        if not context.guild:
            await context.send("`midnight` cannot be done via private message.")
            return

        author = context.message.author
        for gci in self.active_gcis:
            if gci['user'] == author.id:
                await self.midnight(author)
                duration = self.duration(gci)
                await self.save_user_duration(author.id, duration)
                await context.send("{}, Signing off.".format(author.name))
                log("Signed user {} off after {} seconds.".format(author.name, duration))
                return
        await context.send("You weren't signed up as a GCI.")


    @_gci.command(name="sunrise", no_pm=True, aliases=["SUNRISE"])
    async def _sunrise(self, context, server: str, *, remarks: str):
        """Adds you to the list of Active GCIs
        The following must be provided:
        server - The server you are GCIing on.
        remarks - Your callsign + anything related to your GCI slot (frequencies, missions)
        """
        if not context.guild:
            await context.send("`sunrise` cannot be done via private message.")
            return

        author = context.message.author
        server = server.upper()
        remarks = remarks.replace("\n", " - ")
        if server not in GCI_SERVERS:
            await context.send("Unknown server: {}. Possible options are: {}".format(server, ", ".join(GCI_SERVERS)))
            return
        await self.sunrise(author, server, remarks)
        await context.send("Added {} as GCI on {} with remarks [{}]".format(author.mention, server, remarks))

    @_gci.command(name="duration")
    async def _duration(self, context):
        """Checks how long you have spent in the GCI role.
        These stats are updated when you sign off, so it does not include your
        current tenure."""
        author = context.message.author
        if author.id in self.stats:
            duration_seconds = self.stats[author.id]
            log("{} has {} seconds of GCI time".format(author.name, duration_seconds))
            duration_string = math.floor(duration_seconds / 60)
            await context.send("I've recorded {} minutes of you as GCI".format(duration_string))
        else:
            await context.send("I haven't got any record of your time as GCI.")

    @_gci.command(name="duration_list", no_pm=True)
    @checks.mod()
    async def _durationlist(self, context):
        """Spits out a "leaderboard" for the gcis"""
        gcis = {k: v for k, v in sorted(self.stats.items(), key=lambda item: item[1], reverse=True)}
        entries = []
        for gci in gcis:
            user = await self.get_member_by_user_id(gci)
            username = user.name if user else "(no longer here)"
            duration = gcis[gci]
            msg = "{}: {:.1f} hours".format(username, duration / 360)
            entries.append(msg)
            if len(entries) >= 15:
                break
        message = """```{}```""".format("\n".join(entries))
        await context.send(message)



def setup(bot):
    bot.add_cog(GCI(bot))

def log(s):
    print("[GCI]: {}".format(s))
