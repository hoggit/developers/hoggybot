import asyncio
import aiohttp
import discord
import hashlib
import json
import os
import sys
import time
from redbot.core import Config, checks, commands, utils
from redbot.core.commands import Cog, Command
from redbot.core.utils import chat_formatting

DCS_GAME_ID = 313331


class StreamMonitor(Cog):
    """
    Track stream status
    """

    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()
        self.killSwitch = False
        asyncio.ensure_future(self.start_monitor())
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "details": {
                "channel": -1,
                "message": -1,
                "client": "",
                "secret": "",
            }
        })
        self.auth = None

    async def start_monitor(self):
        asyncio.ensure_future(self._poll())

    def __unload(self):
        log("Setting killswitch to True!")
        self.killSwitch = True

    async def update_auth_token(self, client: str, secret: str):
        self.auth = None
        args = {
            "client_id": client,
            "client_secret": secret,
            "grant_type": "client_credentials",
        }
        url = "https://id.twitch.tv/oauth2/token?" + "&".join([f"{k}={v}" for k, v in args.items()])
        response = await self.session.post(url)
        detail = await response.json()
        if response.status != 200:
            log("failed_url:", url)
            log("stream auth:", response.status, await response.json())
            self.auth = None
            return

        detail["expires_at"] = time.time() + detail["expires_in"]
        self.auth = detail

    def makeRequest(self, client, auth_token):
        url = f"https://api.twitch.tv/helix/streams?game_id={DCS_GAME_ID}&first=100"
        headers = {
            'Authorization': 'Bearer ' + auth_token,
            'Client-ID': str(client),
        }
        return self.session.get(url, headers=headers)

    async def _poll(self):

        async def attempt():
            channel_id = await self.config.get_raw("details", "channel")
            message_id = await self.config.get_raw("details", "message")
            if -1 in (channel_id, message_id):
                log("channel needs to be (re)set")
                return

            client = await self.config.get_raw("details", "client")
            if not client:
                log("missing client id")
                return

            secret = await self.config.get_raw("details", "secret")
            if not secret:
                log("missing client secret")
                return

            if self.auth is None or self.auth["expires_at"] < time.time() + 10:
                log("auth token missing or expired, updating")
                await self.update_auth_token(client, secret)
                if not self.auth:
                    log("failed to get new authentication")
                    return

            token = self.auth["access_token"]
            response = await self.makeRequest(client, token)
            data = (await response.json()).get("data", [])
            channel = await self.bot.fetch_channel(channel_id)
            message = await channel.fetch_message(message_id)
            await message.edit(content=self.format_results(data))

        if self.killSwitch:
            log("Killswitch enabled. Stopping polling")
            return
        try:
            await attempt()
        finally:
            await asyncio.sleep(60)
            asyncio.ensure_future(self._poll())

    def format_results(self, streams):
        if len(streams) == 0:
            return "No streams currently online"

        live_dcs_streams = list(filter(lambda s: s['type'] == 'live' and s['game_name'] == 'DCS World', streams))
        live_dcs_streams.sort(key=lambda s: s["started_at"])
        message = 'Found {} streams online: \n'.format(len(live_dcs_streams))
        for stream in live_dcs_streams:
            stream_name = stream["user_name"]
            stream_url = "https://www.twitch.tv/" + stream["user_login"]
            message += "{} - <{}>\n".format(stream_name, stream_url)

        return chat_formatting.escape(message, formatting=True)

    @commands.group(name="streammon", no_pm=True, invoke_without_command=True)
    async def _streammon(self, context):
        """Stream monitor config"""
        await context.send_help()

    @_streammon.command(name="channel_id", no_pm=True)
    @checks.is_owner()
    async def _channel(self, context, channel_id):
        """ Set channel ID """
        if channel_id.startswith("<#"):
            channel_id = channel_id[2:-1]  # strip <#  >
        channel_id = int(channel_id)
        channel = self.bot.get_channel(channel_id)
        await self.config.set_raw("details", "channel", value=channel_id)
        msg = await channel.send("Stream Alerts Enabled")
        await self.config.set_raw("details", "message", value=msg.id)
        await context.send("Stream alerting channel set to {}".format(channel))

    @_streammon.command(name="client_id", no_pm=True)
    @checks.is_owner()
    async def _setClientId(self, context, clientId):
        """ Set client ID """
        await self.config.set_raw("details", "client", value=clientId)
        await context.send("Updated client Id")

    @_streammon.command(name="secret", no_pm=True)
    @checks.is_owner()
    async def _set_client_secret(self, context, secret):
        """ Set client secrent """
        await self.config.set_raw("details", "secret", value=secret)
        await context.send("Updated client secret")

def log(*s):
    print("StreamMonitor: {}".format(" ".join(s)))

def setup(bot):
    bot.add_cog(StreamMonitor(bot))
