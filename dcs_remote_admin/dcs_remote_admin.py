import asyncio
import discord
from redbot.core import Config, commands, checks
from redbot.core.commands import Cog
from redbot.core.utils.chat_formatting import pagify
import json
import aiohttp
import datetime
import hashlib
import os
from bs4 import BeautifulSoup

ALL_SERVERS = "all"

class NoResponseError(RuntimeError):
    pass


class DCSRemoteAdminServer():

    def __init__(self, name, auth, host):

        self.name = name
        self.auth = auth
        self.host = host

    @staticmethod
    def fromConfig(serverDict):

        return DCSRemoteAdminServer(
            serverDict["name"],
            serverDict["auth"],
            serverDict["host"]
        )

    @staticmethod
    def withName(name):
        return DCSRemoteAdminServer(name, "", "")

    def withAuth(self, auth):
        return DCSRemoteAdminServer(
            self.name,
            auth,
            self.host
        )

    def withHost(self, host):
        return DCSRemoteAdminServer(
            self.name,
            self.auth,
            host
        )


class DCSPlayer():

    def __init__(self, id, name, ucid):
        self.id = id
        self.name = name
        self.ucid = ucid

    @staticmethod
    def fromJson(js):
        return DCSPlayer(
            js["id"],
            js["name"],
            js["ucid"]
        )

class DCSRemoteAdmin(Cog):
    """
    Handles DCS Remote Administration
    """

    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "servers": {}
        })

    def __unload(self):
        self.session.close()

    @commands.group()
    async def dcsadmin(self, context):
        if context.invoked_subcommand is None:
            return

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def players(self, context, servername):
        servers = await self.config.servers()
        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        try:
            jsPlayers = await self.getPlayerList(server)
        except NoResponseError:
            await context.send(f"Timed out trying to access {servername}")
            return
        players = map(DCSPlayer.fromJson, jsPlayers)
        response = []
        for player in players:
            response.append(f"{player.id}: {player.name} [{player.ucid}]")
        response = "```{}```".format("\n".join(response))
        await context.send(response)


    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def addserver(self, context, servername):
        """Adds a new server to the admin system.
        Will error if the server already exists."""
        if servername == ALL_SERVERS:
            await context.send(f"Cannot add '{ALL_SERVERS}', it's used to refer to all servers")
            return
        servers = await self.config.servers()
        if servername in servers:
            await context.send(f"Server {servername} already exists")
            return
        new_server = DCSRemoteAdminServer.withName(servername)
        await self.config.set_raw("servers", servername, value=new_server.__dict__)
        await context.send(f"Added {servername} to serverlist.")

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def auth(self, context, servername, auth_token):
        """Adds an auth token to use for connecting to the dcs admin server"""
        servers = await self.config.servers()
        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist. Please add it with `addServer`.")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        server = server.withAuth(auth_token)
        await self.config.set_raw("servers", servername, value=server.__dict__)
        await context.send(f"Added auth token to server: {servername}")

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def host(self, context, servername, hostname):
        """Adds a hostname to access the remote dcs admin server"""
        servers = await self.config.servers()
        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist. Please add it with `addServer`.")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        server = server.withHost(hostname)
        await self.config.set_raw("servers", servername, value=server.__dict__)
        await context.send(f"Added hostname to server: {servername}")

    @dcsadmin.command(alias = "list")
    @checks.mod_or_permissions(manage_guild=True)
    async def list_servers(self, context):
        servers = await self.config.servers()
        for servername in servers:
            server = DCSRemoteAdminServer.fromConfig(servers[servername])
            await context.send(f"{server.name}: {server.host}")

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def chat(self, context, servername, *, chat_message):
        servers = await self.config.servers()
        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist. Please add it with `addServer`.")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        await self.sendChat(server, chat_message)
        await context.message.add_reaction('✅')

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def banucid(self, context, servername, ucid):
        """ Issues an SLMod ucid ban for the provided ucid"""
        servers = await self.config.servers()
        if servername == ALL_SERVERS:
            for server in servers:
                await self.banucid(context, server, ucid)
            return

        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist. Please add it with `addServer`.")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        message =f"-admin ucidban {ucid}"
        await self.sendChat(server, message)
        await context.message.add_reaction('✅')

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def ban(self, context, servername, *, playerToBan):
        """Issues an SLMod ban to the provided player name."""
        servers = await self.config.servers()
        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist. Please add it with `addServer`.")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        message = f"-admin ban {playerToBan}"
        await self.sendChat(server, message)
        await context.message.add_reaction('✅')

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def unban(self, context, servername, player):
        """ Removes an SLMod ucid ban for the provided ucid"""
        servers = await self.config.servers()
        if servername == ALL_SERVERS:
            for server in servers:
                await self.unban(context, server, player)
            return

        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist. Please add it with `addServer`.")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        message =f"-admin unban {player}"
        await self.sendChat(server, message)
        await context.message.add_reaction('✅')

    @dcsadmin.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def alert(self, context, servername, *, alert_message):
        """Creates an outText message box containing the provided alert_message"""
        servers = await self.config.servers()
        if not servername in servers:
            await context.send(f"Server {servername} doesn't exist. Please add it with `addServer`.")
            return
        server = DCSRemoteAdminServer.fromConfig(servers[servername])
        message =f"-admin alert {alert_message}"
        await self.sendChat(server, message)
        await context.message.add_reaction('✅')


    async def getPlayerList(self, dcs_admin_server):
        url = f"https://{dcs_admin_server.host}/players"
        headers = {
            "Auth": f"Bearer {dcs_admin_server.auth}",
            "User-Agent": "HoggyBot_dcs_remote_admin"
            }
        resp = await self.session.get(url, headers = headers)
        resp_text = await resp.text()
        if not resp_text:
            raise NoResponseError()
        playersList = json.loads(resp_text)
        return playersList

    async def sendChat(self, dcs_admin_server, chat_message):
        url = f"https://{dcs_admin_server.host}/chat"
        headers = {
            "Auth": f"Bearer {dcs_admin_server.auth}",
            "User-Agent": "HoggyBot_dcs_remote_admin",
            "Content-Type": "application/json"
        }
        response = await self.session.post(url, headers=headers, data=f"\"{chat_message}\"")
        return await response.text()



def setup(bot):
    bot.add_cog(DCSRemoteAdmin(bot))
