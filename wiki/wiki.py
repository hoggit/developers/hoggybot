import asyncio
import os
import aiohttp
import discord
import arrow
import hashlib
import json
import sys
import urllib.parse
from bs4 import BeautifulSoup
from redbot.core.utils.chat_formatting import pagify
from redbot.core import Config, checks, commands
from redbot.core.commands import Cog
from urllib.parse import urlencode


class HoggitWiki(Cog):
    """
    Search the hoggit wiki
    """

    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()
        self.killSwitch = False
        self.base_url = "https://wiki.hoggitworld.com"
        self.recent_changes_url = self.base_url + "/api.php?action=query&list=recentchanges&rcprop=user|title|timestamp&format=json&rctype=edit"
        self.last_wiki_check = arrow.utcnow()
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "alerts": {
                "channel": -1,
            },
            "synonyms": {}
        })
        asyncio.ensure_future(self.start_alerts())

    def __unload(self):
        #Needed to not reschedule the next check.
        self.killswitch = True

    async def start_alerts(self):
        await self.bot.wait_until_red_ready()
        alert_channel = await self.config.get_raw("alerts", "channel")
        if alert_channel == -1:
            print("Wiki: No alerts channel set")
        else:
            print("Wiki: Starting alerts for channel: " + self.bot.get_channel(alert_channel).name)
            asyncio.ensure_future(self._alert(alert_channel))

    def format_recent_changes(self, results):
        embed=discord.Embed(title="Wiki changes since {}".format(self.last_wiki_check.humanize()))
        print(results)
        results_length = len(results)
        trimmed_results = results[:5]
        for result in trimmed_results:
            url = self.base_url + "/view/" + urllib.parse.quote(result["title"])
            embed.add_field(
                    name=result["title"],
                    value="{} - {}".format(result["user"], arrow.get(result["timestamp"]).humanize()))

        if results_length > 5:
            embed.set_footer(text="And {} more changes omitted".format(results_length - 5))
        return embed


    async def _alert(self, chan_id: int):
        await asyncio.sleep(3600) #1 hour
        if self.killSwitch:
            return
        timestamp = self.last_wiki_check.format('YYYY-MM-DDTHH:mm:ss')
        url = self.recent_changes_url + "&rcend=" + timestamp
        try:
            response = await self.session.get(url)
            recent_changes = json.loads(await response.text())
            results = recent_changes["query"]["recentchanges"]
            if results:
                formatted_results = self.format_recent_changes(results)
                self.last_wiki_check = arrow.utcnow()
                channel = self.bot.get_channel(chan_id)
                await channel.send(embed=formatted_results)
        except Exception as e:
            print("Wiki: Unexpected error sending wiki recent changes: " + e)
        finally:
            if await self.config.get_raw("alerts", "channel") == chan_id and not self.killSwitch:
                asyncio.ensure_future(self._alert(chan_id))

    def url(self, search):
        return self.base_url + "/index.php?title=Special%3ASearch&search={}&go=Go".format(search)

    @staticmethod
    def was_redirect(resp):
        return len(resp.history) > 0

    async def bot_say_single_result(self, context, result_url):
        message = "<{}>".format(result_url)
        await context.send(message)

    async def parse_results(self, response):
        soup = BeautifulSoup(await response.text())
        search_results = soup.find_all("div", "mw-search-result-heading")
        results_parsed = 0
        max_results = 3
        parsed_results = []
        links_used = []
        for ele in search_results:
            sr = ele.find("a")
            result = dict()
            result["title"] = sr["title"]
            result["link"] = self.base_url + sr["href"]
            if result["link"] not in links_used:
                parsed_results.append(result)
                links_used.append(result["link"])
                results_parsed += 1
                if results_parsed >= max_results:
                    break

        return parsed_results

    @staticmethod
    def format_results(results):
        formatted_results = []
        for result in results:
            formatted = "{}: <{}>".format(result["title"], result["link"])
            formatted_results.append(formatted)
        return formatted_results

    async def bot_say_search_results(self, context, response):
        results = await self.parse_results(response)
        formatted_results = HoggitWiki.format_results(results)
        if len(formatted_results) == 0:
            message = "Could not find any results :("
        else:
            message = "I couldn't find an exact match. But here's some suggestions:\n{0}\n".format(
                    "\n".join(str(x) for x in formatted_results))
        await context.send(message)

    @commands.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def wiki_syn(self, context, command, *args):
        """ Adds or removes a synonym for a search
            - add <synonym to add> > <target>
            - remove <synonym to remove>
            - list
        """
        if command == "list":
            current_synonyms = await self.config.get_raw("synonyms")
            if not current_synonyms:
                await context.send("No synonyms exist yet")
                return
            items = ["%s: %s" % (k, v) for k, v in current_synonyms.items()]
            await context.send("Current synonyms:\n " + "\n  ".join(items))
            return

        if command == "remove":
            syn = " ".join(args)
            existing = await self.config.get_raw("synonyms", syn, default=None)
            if not existing:
                await context.send("Synonym {0} not found.".format(syn))
            else:
                target = await self.config.get_raw("synonyms", syn)
                await self.config.clear_raw("synonyms", syn)
                await context.send("Synonym {0} -> {1} removed".format(syn, target))
            return

        query = ' '.join(args)
        if ">" not in query:
            await context.send("Use the format: new synonym > old synonym")
            return
        syn, target = [i.strip() for i in query.split(">", 1)]
        if ">" in target:
            await context.send("Too many '>' in target ")
            return
        syn = syn.lower()
        if command == "add":
            await self.config.set_raw("synonyms", syn, value=target)
            await context.send("Synonym {0} -> {1} added".format(syn, target))
            return
        await context.send_help()


    @commands.command(name="wiki")
    async def wiki(self, ctx, *search_text):
        """ Searches the hoggit wiki for the given text """
        if not search_text:
            await ctx.send_help()
            return
        query = ' '.join(search_text)
        query = await self.config.get_raw("synonyms", query.lower().strip(), default=query)

        resp = await self.session.get(self.url(query))
        if HoggitWiki.was_redirect(resp):
            await self.bot_say_single_result(ctx, resp.url)
        else:
            await self.bot_say_search_results(ctx, resp)

    @commands.command(name="embed-test")
    @checks.mod_or_permissions(manage_guild=True)
    async def embed_test(self, context):
        channel = self.bot.get_channel(await self.config.get_raw("alerts", "channel"))
        if channel == -1:
            context.send("Unable: no target channel configured")
            return
        embed=discord.Embed()
        embed.add_field(name="[F/A-18C](http://www.google.com)", value="Acidictadpole - 3 minutes ago", inline=False)
        await channel.send(embed=embed)

    @commands.command(name="wiki-alert")
    @checks.mod_or_permissions(manage_guild=True)
    async def alert(self, context, channel_id):
        """
        Configures alerts for the hoggit wiki to be sent to the given channel.

        `channel_id` must be a channel that the bot can send messages to
        """
        if channel_id.startswith("<#"):
            channel_id = channel_id[2:-1]  # strip <#  >
        channel_id = int(channel_id)
        channel = self.bot.get_channel(channel_id)
        await self.config.set_raw("alerts", "channel", value=channel_id)
        print("Wiki: New alert requested for channel {}".format(channel.name))
        await self.start_alerts()
        await context.send("Started an alert for #{}".format(channel.name))

def setup(bot):
    # if not os.path.exists("data/wiki"):
        # print("Wiki: Creating data/wiki folder...")
        # os.makedirs("data/wiki")

    # f = "data/wiki/synonyms.json"
    # if not fileIO(f, "check"):
        # print("Wiki: Creating empty synonyms.json...")
        # fileIO(f, "save", {})

    # f = "data/wiki/alerts.json"
    # if not fileIO(f, "check"):
        # print("Wiki: Creating empty alerts.json...")
        # dataIO.save_json(f, {})

    bot.add_cog(HoggitWiki(bot))
