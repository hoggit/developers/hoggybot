import asyncpraw
import asyncio
import re
import hashlib
from discord import TextChannel, Embed
from redbot.core.commands import Cog
from redbot.core import Config, checks, commands


class RedditNotConfigured(Exception):
    def __init__(self):
        """
        Used for when reddit has not been configured on the bot yet
        """
        return

class HoggitWeeklyQuestions(Cog):
    """
    Relays new top-level comments from the stickied questions thread into a channel
    on discord.
    """

    def __init__(self, bot):
        self.bot = bot
        self.reddit_instance = False
        self.killPoll = False
        asyncio.ensure_future(self.poll())
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "reddit": {
                "clientid": "",
                "clientsecret": "",
                "ignored_users": [],
                "last_utc_check": -1
            },
            "channel_id": -1

        })

    def __unload(self):
        self.killPoll = True

    async def reddit(self):
        if not self.reddit_instance:
            clientid = await self.config.reddit.clientid()
            clientsecret = await self.config.reddit.clientsecret()
            if clientid == "" or clientsecret == "":
                raise RedditNotConfigured()
            self.reddit_instance = asyncpraw.Reddit(client_id=clientid, client_secret=clientsecret, user_agent='praw Hoggybot')
        return self.reddit_instance


    async def weekly_thread(self):
        reddit = await self.reddit()
        subreddit = await reddit.subreddit('hoggit')
        posts = (post async for post in subreddit.hot(limit=2) if post.stickied)
        async for post in posts:
            if re.search("^Weekly Questions Thread", post.title):
                return post
        return None

    async def weekly_questions_comments(self, last_utc_check):
        if not last_utc_check:
            last_utc_check = 0
        thread = await self.weekly_thread()
        thread.comment_sort = 'new'
        await thread.load()
        top_level_comments = thread.comments
        comments = [c for c in top_level_comments if c.created_utc > last_utc_check]
        return comments

    async def say_comments(self, comments):
        channel_id = await self.config.channel_id()
        if channel_id == -1:
            log("Could not post {} comments to a channel, as no channel is configured".format(len(comments)))
            return
        chan = self.bot.get_channel(channel_id)
        if not chan:
            log("Can't find channel with id: {}".format(channel_id))
            return
        ignored_users = await self.config.reddit.ignored_users()
        for comment in comments:
            if comment.author.name not in ignored_users:
                embed = Embed()
                question = comment.body[:250] + "..." if len(comment.body) > 250 else comment.body
                embed.add_field(name="User", value=comment.author.name, inline=False)
                embed.add_field(name="Question", value=question, inline=False)
                embed.add_field(name="Link", value="https://reddit.com{}".format(comment.permalink), inline=False)
                await chan.send(embed=embed)

    async def poll(self):
        try:
            await self.bot.wait_until_ready()
            last_utc = await self.config.reddit.last_utc_check()
            comments = await self.weekly_questions_comments(last_utc)
            if len(comments) > 0:
                last_comment_utc = max(c.created_utc for c in comments)
                await self.config.reddit.last_utc_check.set(last_comment_utc)
                await self.say_comments(comments)
        except RedditNotConfigured:
            log("Reddit not configured. Skipping poll")
        finally:
            if self.killPoll:
                log("Killswitch engaged. Stopping polling")
                return
            await asyncio.sleep(10)
            asyncio.ensure_future(self.poll())

    @commands.group(name="weeklyquestions", pass_context=True, invoke_without_command=True)
    async def _weeklyquestions(self, ctx):
        """
        Lists the current weekly questions thread
        """
        post = await self.weekly_thread()
        if post:
            await ctx.send("This week's question thread: http://reddit.com{}".format(post.permalink))
            return

        await ctx.send("Couldn't find this week's question thread. Is it stickied?")
        return

    @_weeklyquestions.command(name="ignore")
    @checks.is_owner()
    async def _ignore_user(self, context, user:str):
        """
        Ignores a username from the weekly threads.
        Commands by these users are no longer displayed
        """
        async with self.config.reddit.ignored_users() as ignored_users:
            ignored_users.append(user)
        await context.send("Added {} to ignored list".format(user))

    @_weeklyquestions.command(name="channel")
    @checks.is_owner()
    async def _set_channel(self, context, chan: TextChannel):
        """
        Sets the channel to send the questions to
        """
        await self.config.channel_id.set(chan.id)
        await context.send("Set the question channel to {}".format(chan.name))

    @commands.group(name="reddit")
    async def _reddit(self, ctx):
        return

    @_reddit.command(name="clientid")
    @checks.is_owner()
    async def _clientid(self, context, clientid: str):
        await self.config.reddit.clientid.set(clientid)
        await context.send("Reddit Client ID set")

    @_reddit.command(name="clientsecret")
    @checks.is_owner()
    async def _clientsecret(self, context, clientsecret: str):
        await self.config.reddit.clientsecret.set(clientsecret)
        await context.send("Reddit Client Secret set")

def log(msg):
    print("[WeeklyQuestions]--{}".format(msg))
    return

def setup(bot):
    bot.add_cog(HoggitWeeklyQuestions(bot))
