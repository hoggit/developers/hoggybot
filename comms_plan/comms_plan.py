import csv
import discord
import aiohttp
import io
from redbot.core import commands
from redbot.core.commands import Cog


class CommsPlan(Cog):
    """
    Fetches the comms plan from discord
    """

    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()
        #Make configurable.
        self.comms_plan_url = "https://docs.google.com/spreadsheets/d/1a63VD2WXmShIwpiTTfHuK-5yWKw-3AtXLbv1LBjMyCE"
        self.comms_plan_export = self.comms_plan_url + "/export?exportFormat=csv"

    def __unload(self):
        self.session.close()

    async def fetch_comms_plan(self, context):
        resp = await self.session.get(self.comms_plan_export)
        if resp.status != 200:
            await context.send("Error getting comms plan. Check logs")
            raise Exception("Could not get status")
        return await resp.text()

    async def respond_with_plan(self, plan, context):
        message_template = "```Address: use !ip to get the matching address\n%s```%s"
        plan_message = ""
        for radio in plan:
            plan_message += "%s: %s\n" % (radio['use'], radio['freq'])

        try:
            await context.message.author.send(message_template % (plan_message, "Details: <%s>" % self.comms_plan_url))
            await context.message.add_reaction('✅')
        except discord.errors.Forbidden:
            message = "Unable to message you the comms plan you requested. Either you blocked me or you disabled DMs from this server."
            await context.send(message)
        except Exception:
            message = "Something went wrong trying to send you a DM. You can try again or view the comms plan here: " + self.comms_plan_url
            await context.send(message)

    @commands.command(aliases=["srs"])
    async def print_comms_plan(self, ctx):
        """Fetches the latest comms plan for GAW/PGAW. The response will be sent in a Direct Message
        So ensure that you have allowed Hoggy to send you messages privately"""
        comms_plan = await self.fetch_comms_plan(ctx)
        comms_plan = parse_comms_plan(comms_plan)
        await self.respond_with_plan(comms_plan, ctx)


def is_number(number):
    try:
        float(number)
        return True
    except ValueError:
        return False


def parse_comms_plan(plan):
    data = io.StringIO(plan)
    reader = csv.reader(data)
    comms_info = []
    for row in reader:
        if row[1] and is_number(row[1]):
            radio = {}
            radio['freq'] = row[1]
            radio['use'] = row[3]
            comms_info.append(radio)

    return comms_info


def setup(bot):
    bot.add_cog(CommsPlan(bot))
