#!/bin/bash

python3 -m venv .venv

echo "Created virtualenv at .venv"
source .venv/bin/activate

echo "Installing redbot dependencies..."
python -m pip install -U pip setuptools wheel

echo "Installing Redbot"
python -m pip install -U Red-DiscordBot

redbot-setup