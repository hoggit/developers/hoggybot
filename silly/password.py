import asyncio
import discord
import hashlib
import random

from redbot.core import Config, checks, commands
from redbot.core.commands import Cog


class Password(Cog):
    """
    Handles the password access commands for the DCS servers
    """

    def __init__(self, bot):
        self.bot = bot
        # asyncio.ensure_future(self.start_monitor())
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "details": {
                "nominated": [],
            }
        })


    @commands.group(name="password-config", invoke_without_command=True, case_insensitive=True)
    @checks.is_owner()
    async def _password_config(self, context, *args):
        """Password config"""
        await context.send_help()

    @_password_config.command(name="nominate", no_pm=True)
    @checks.is_owner()
    async def _nominate(self, context, *user_ids):
        """ Nominate one or more users to be pinged for passwords """
        for user_id in user_ids:
            if user_id.startswith("<@"):
                uid = int(user_id[2:-1].lstrip("!"))  # strip <@!  >
            elif not user_id.isdigit():
                await context.send("Invalid ID or mention: " + user_id)
                return
            else:
                uid = int(user_id)
            if not await context.guild.fetch_member(uid):
                await context.send("Cannot find user: " + user_id)
                return

            current = list(await self.config.get_raw("details", "nominated"))

            if uid not in current:
                current.append(uid)
            while len(current) > 3:
                current.pop(0)
            await self.config.set_raw("details", "nominated", value=current)

        current = await self.config.get_raw("details", "nominated")
        names = []
        for uid in current:
            member = await context.guild.fetch_member(uid)
            if member:
                names.append(member.name)
        if not names:
            await context.send("No valid users in the pool")
        else:
            await context.send("Users in the pool are: " + (", ".join(names)))

    @_password_config.command(name="clear", no_pm=True)
    @checks.is_owner()
    async def _clear(self, context):
        """ Remove all nominated users """
        await self.config.set_raw("details", "nominated", value=[])
        await context.message.add_reaction('✅')

    @commands.command("pass-of-word", case_insensitive=True, no_pm=True)
    async def _pass_of_word(self, context):
        """ Asks for the server password """
        author_id = context.message.author.id
        current = list(await self.config.get_raw("details", "nominated"))
        if not current:
            await context.send("Try `!word-of-pass` instead.")
            await self.config.set_raw("details", "nominated", value=[author_id])
            return
        selection = random.randrange(len(current))
        uid = current[selection]
        if author_id == uid:
            await context.send("Try `!word-of-pass` instead.")
            return
        if random.random() >= 0.5:
            if len(current) == 3:
                current.pop(selection)
            if author_id not in current:
                current.append(author_id)
            await self.config.set_raw("details", "nominated", value=current)
        base = "<@!{}>, <@!{}> would like the password to the servers."
        await context.send(base.format(uid, author_id))
        

def log(s):
    print("Password: {}".format(s))

def setup(bot):
    bot.add_cog(Password(bot))
