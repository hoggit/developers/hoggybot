import asyncio
import discord
from redbot.core import Config, commands, checks
from redbot.core.commands import Cog
from redbot.core.utils.chat_formatting import pagify
import json
import aiohttp
import datetime
import hashlib
import os
import arrow
from bs4 import BeautifulSoup

class ErrorGettingStatus(Exception):
    def __init__(self, statusCode):
        self.status=statusCode

class ServerHealth:
    """
    Returns a ServerHealth with a health status string indicating "Online", "Unhealthy", "Offline"
    and a `color` for use in graphics.
    Green - Online
    Orange - Unhealthy
    Red - Offline
    """

    def __init__(self, updateTime, server_key, data):
        self.uptime_data = data
        self.status = self.determine_status(updateTime, self.uptime_data[server_key], server_key)
        self.color = self.determine_color(self.status)
        self.uptime = self.determine_uptime(self.status, self.uptime_data[server_key])

    def determine_status(self, updateTime, uptime_data, server_key):
        now = arrow.utcnow()
        status = "Online"
        if "status" not in uptime_data:
            uptime_data["status"] = ""
        if (updateTime < now.shift(seconds=-60)):
            status = "Unhealthy"
        if (updateTime < now.shift(seconds=-100)):
            status = "Offline"
        if status != uptime_data["status"]:
            self.store_uptime(status, updateTime, server_key)
        return status

    def determine_color(self, status):
        if status == "Online":
            return 0x05e400
        if status == "Unhealthy":
            return 0xFF9700
        return 0xFF0000

    def determine_uptime(self, status, uptime_data):
        now = arrow.utcnow()
        if "status" not in self.uptime_data:
            return self.determine_delta(now, uptime_data["time"])
        if self.uptime_data['status'] == status:
            current_uptime = self.determine_delta(now, uptime_data["time"])
            return current_uptime

    def store_uptime(self, status, time, server_key):
        self.uptime_data[server_key]["status"] = status
        self.uptime_data[server_key]["time"] = time.for_json()

    def determine_delta(self, current, change):
        delta = current - arrow.get(change)
        days = delta.days
        hours,remainder = divmod(delta.seconds,3600)
        minutes,seconds = divmod(remainder,60)
        return "{0} hours {1} minutes {2} seconds".format(
            hours, minutes, seconds
        )

class DCSServerStatus(Cog):
    """
    Returns the status of your DCS server
    """

    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()
        self.config = Config.get_conf(self, int(hashlib.sha512(("hoggit." + self.__class__.__name__).encode()).hexdigest(), 16))
        self.config.register_global(**{
            "servers": {},
            "watches": {}
        })
        self.server_watches = None
        self.base_url = "https://status.hoggitworld.com/"
        self.killPoll = False
        self.last_key_checked = None
        self.start_polling()
        self.presence_cycle_time_seconds = 5
        asyncio.ensure_future(self.load_config())

    async def load_config(self):
        self.server_watches = await self.config.get_raw("watches")

    def __unload(self):
        #kill the polling
        self.killPoll = True
        self.session.close()

    def start_polling(self):
        asyncio.ensure_future(self.poll())
        print("Server Status polling started")

    async def get_next_key(self):
        key = None
        servers = sorted(await self.config.get_raw("servers"))
        key = self.last_key_checked
        try:
            key = servers[(servers.index(key) + 1) % len(servers)]
        except ValueError:
            key = servers[0]
        self.last_key_checked = key
        return key

    async def poll(self):
        try:
            if self.killPoll:
                print("Server Status poll killswitch received. Not scheduling another poll")
                return
            key = await self.get_next_key()
            if not key:
                return #still runs finally
            data = await self.config.get_raw("servers", key)
            status = await self.get_status(data["key"])
            await self.checkWatches(status, data["alias"].lower())
            await self.set_presence(status, key)
        except Exception as e:
            print("Server Status poll encountered an error. skipping this poll: ", repr(e))
        finally:
            await asyncio.sleep(self.presence_cycle_time_seconds)
            asyncio.ensure_future(self.poll())

    async def store_key(self, key):
        await self.config.set_raw("servers", key["alias"].lower(), value=key)

    async def delete_key(self, alias):
        alias = alias.lower()
        await self.config.clear_raw("servers", alias)
        await self.config.clear_raw("watches", alias)

    async def set_presence(self, status, server_key):
        server_data = await self.config.get_raw("servers", server_key)
        game="{} players on {} playing on {}".format(status["players"], server_data["alias"], status["missionName"])
        health = await self.determine_health(status, server_key)
        bot_status=discord.Status.online
        if health.status == "Unhealthy":
            bot_status=discord.Status.idle
            game="Slow updates - " + game
        elif health.status == "Offline":
            bot_status=discord.Status.dnd
            game="{} Server offline".format(server_data["alias"])
        await self.bot.change_presence(status=bot_status, activity=discord.Game(name=game))

    async def get_status(self, key):
        url = self.base_url + key
        resp = await self.session.get(url)
        if (resp.status != 200):
            raise ErrorGettingStatus(resp.status)
        status = json.loads(await resp.text())
        #Hoggy Server counts himself among the players, except if it's not running
        if status["maxPlayers"] >= 1:
            status["players"] = status["players"] - 1
            status["maxPlayers"] = status["maxPlayers"] - 1
        return status

    async def determine_health(self, status, server_key):
        last_update = arrow.get(status["data"]["updateTime"])
        return ServerHealth(last_update, server_key, await self.config.get_raw("servers"))

    def humanize_time(self, updateTime):
        arrowtime = arrow.get(updateTime)
        return arrowtime.humanize()

    def get_mission_time(self, status):
        time_seconds = datetime.timedelta(seconds=status["data"]["uptime"])
        return str(time_seconds).split(".")[0]

    def get_metar(self, status):
        if "metar" in status["data"]:
            metar = status["data"]["metar"]
            if metar:
                return metar
        return "Unavailable"

    def get_dcs_version(self, status):
        if "dcsVersion" in status["data"]:
            dcsVersion = status["data"]["dcsVersion"]
            if dcsVersion:
                return dcsVersion
        return "Unavailable"

    async def embedMessage(self, status, alias):
        health = await self.determine_health(status, alias)
        embed = discord.Embed(color=health.color)
        embed.set_author(name=status["serverName"], icon_url="https://i.imgur.com/KEd7OQJ.png")
        embed.set_thumbnail(url="https://i.imgur.com/KEd7OQJ.png")
        embed.add_field(name="Status", value=health.status, inline=True)
        embed.add_field(name="Mission", value=status["missionName"], inline=True)
        embed.add_field(name="Map", value=status["map"], inline=True)
        embed.add_field(name="Players", value="{}/{}".format(status["players"], status["maxPlayers"]), inline=True)
        embed.add_field(name="METAR", value=self.get_metar(status))
        if health.status == "Online":
            embed.add_field(name="Mission Time", value=self.get_mission_time(status), inline=True)
        else:
            embed.add_field(name="{} Since".format(health.status), value=health.uptime, inline=True)
        embed.add_field(name="DCS Version", value=self.get_dcs_version(status), inline=True)
        embed.set_footer(text="Last update: {} -- See my status light for up-to-date status.".format(self.humanize_time(status["updateTime"])))
        return embed

    async def embedMessageShort(self, status, alias):
        health = await self.determine_health(status, alias)
        embed = discord.Embed(color=health.color)
        embed.set_author(name=status["serverName"], icon_url="https://i.imgur.com/KEd7OQJ.png")
        embed.add_field(name="Players", value="{}/{}".format(status["players"], status["maxPlayers"]), inline=True)
        if health.status == "Online":
            embed.add_field(name="Mission Time", value=self.get_mission_time(status), inline=True)
        else:
            embed.add_field(name="{} Since".format(health.status), value=health.uptime, inline=True)
        embed.set_footer(text="DCS Version: {}".format(self.get_dcs_version(status)))
        return embed

    @commands.command(name="serverlist")
    async def _servers(self, context):
        """Displays the list of tracked servers"""
        servers = await self.config.get_raw("servers")
        if not servers:
            await context.send("No servers currently being tracked")
            return
        message = "Tracking the following servers:\n"
        for key,server in servers.items():
            message += ":desktop: -- {}\n".format(server["alias"])
        message += "\n\nUse `!server <servername>` to get the status of that server"
        await context.send(message)

    @commands.command(name = "server")
    async def server_status(self, context, alias):
        """Gets the server status for the provided alias. Use !serverlist to see all the servers we're tracking"""
        blocked = "Unable to message you the details of the server you requested. Either you blocked me or you disabled DMs from this server."
        async def respond_in_pm(text: str = None, embed = None) -> bool:
            if text is None and embed is None:
                raise ValueError("nothing to respond with")
            try:
                await context.message.author.send(text, embed=embed)
            except discord.http.Forbidden:
                # don't attempt to PM immediately after PMing fails due to being blocked
                if context.guild:
                    await context.send(blocked)
                    return False
            return True

        if context.guild:
            if not await respond_in_pm("Please only use `!server` in PMs with me."):
                # abort early, since the rest won't go through either
                return

        alias = alias.lower()
        server = (await self.config.get_raw("servers")).get(alias)
        if not server:
            await respond_in_pm("We aren't tracking a server called {}".format(alias))
            return

        try:
            status = await self.get_status(await self.config.get_raw("servers", alias, "key"))
            message = await self.embedMessage(status, alias)
            try:
                await respond_in_pm(embed=message)
            except discord.errors.HTTPException:
                print("Server status failed to send message:", message, "from", status)
                raise
            try:
                await context.message.add_reaction('✅')
            except discord.http.Forbidden:  # if blocked by a user, this would also fail
                pass
        except ErrorGettingStatus as e:
            await respond_in_pm("Status unknown right now.")
            print("Error getting status. Response code was " + str(e.status))

    @commands.command(name="tickets", no_pm=True)
    async def tickets(self, context):
        """Gets the current tickets for a server"""
        server = "SAW"
        bucket_size = 20
        server = server.upper()
        try:
            status = await self.get_status(await self.config.get_raw("servers", server.lower(), "key"))
            tickets = status.get("data", {}).get("tickets", {}).get("Blue", {})
            current = tickets.get("Current", -1)
            start = tickets.get("Start", -1)
            if start == -1:
                message = "No ticket system in place for %s." % server
            elif start == -2:
                message = "Waiting on ticket state for %s, try again later." % server
            else:
                lower = (current // bucket_size) * bucket_size
                if lower < bucket_size:
                    current = "< %d" % bucket_size
                else:
                    current = "%d to %d" % (lower, lower + bucket_size)
                message = "Players have %s tickets left on %s" % (current, server)
            try:
                await context.send(message)
            except discord.errors.HTTPException:
                print("Server status failed to send message:", message, "from", status)
        except ErrorGettingStatus as e:
            await context.send("Server status unknown right now.")
            print("Error getting status. Response code was " + str(e.status))

    async def saveWatches(self, watches):
        await self.config.set_raw("watches", value=watches)

    async def checkWatches(self, server_status, alias):
        watches = self.server_watches
        health = await self.determine_health(server_status, alias)
        if (server_status["players"] == 0 or health.status == "Offline") and alias in self.server_watches:
            for watcherId in self.server_watches[alias]:
                watcher = await self.bot.fetch_user(watcherId)
                await watcher.send("Server [{}] has reached 0 players!".format(alias))
            self.server_watches[alias] = []
            await self.saveWatches(self.server_watches)

    async def watchForServer(self, author_id, server:str):
        if server not in self.server_watches:
            self.server_watches[server] = []
        if author_id not in self.server_watches[server]:
            self.server_watches[server].append(author_id)
            await self.saveWatches(self.server_watches)

    @commands.command(name = "serverwatch")
    async def _server_watch(self, context, alias: str):
        """Creates a watch on server status. You will be notified when the server reaches zero population"""
        alias = alias.lower()
        servers = await self.config.get_raw("servers")
        if alias == 'all':
            for server in servers:
                await self.watchForServer(context.message.author.id, server)
        else:
            if alias not in servers:
                await context.message.add_reaction('❌')
                return
            await self.watchForServer(context.message.author.id, alias)
        await context.message.add_reaction('✅')


    @commands.command(aliases=["servershort"])
    async def _server_status_short(self, context):
        if context.guild:
            await context.send("This command must be in a PM")
            return
        servers = await self.config.get_raw("servers")
        if not servers:
            context.send("No servers configured")
        for name, server in servers.items():
            status = await self.get_status(server["key"])
            message = await self.embedMessageShort(status, server["alias"].lower())
            await context.send(embed=message)


    @commands.group(aliases=["serverconf"])
    async def _serverconf(self, context):
        if context.invoked_subcommand is None:
            return

    @_serverconf.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def delete(self, context, alias):
        await self.delete_key(alias)
        await context.send("Removed key for {}".format(alias))

    @_serverconf.command()
    @checks.mod_or_permissions(manage_guild=True)
    async def key(self, context, alias, *, text = ""):
        key = {
            "key": text,
            "alias": alias,
        }
        await self.store_key(key)
        await context.send("Updated key for {alias} to {key}".format(**key))

def setup(bot):
    bot.add_cog(DCSServerStatus(bot))
